from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^auth/$', views.auth, name='auth'),
    url(r'^register/$', views.register, name='register'),
    url(r'^register/api/$', views.ApiView.as_view(), name='register_api'),
    url(r'^logout/$', views.logout, name='logout'),
]

import json
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.views.decorators.cache import never_cache
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.http import HttpResponseRedirect
from django.views import View
from .service import RegistrationService
from .forms import LoginForm
from .tools import rnd_string


def index(request):
    return render(request, 'registration/index.html')


@never_cache
def auth(request):
    if request.method == "POST":
        form = LoginForm(request, data=request.POST)
        if form.is_valid():
            auth_login(request, form.get_user())
    else:
        form = LoginForm(request)

    if request.user.id is not None:
        return HttpResponseRedirect(reverse('registration:register'))

    return render(request, 'registration/auth.html', {
        'form': form
    })


def register(request):
    return render(request, 'registration/registration.html', {
        'random_string': rnd_string()
    })


@never_cache
def logout(request):
    auth_logout(request)
    return HttpResponseRedirect(reverse('registration:index'))


class ApiView(View):

    def get(self, request):
        return render(request, 'registration/registration.json.tpl', {
            'user': request.user,
            'message': 'ok'
        })

    def post(self, request):
        data = json.loads(request.body)
        message = 'ok'
        try:
            company = RegistrationService().process_data(data)
            auth_login(request, company.user, 'apps.registration.backends.EmailAuthBackend')
        except ValidationError as err:
            message = err.message
        return render(request, 'registration/registration.json.tpl', {
            'user': request.user,
            'message': message
        })

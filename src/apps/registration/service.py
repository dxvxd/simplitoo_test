from django.core.validators import validate_email, ValidationError
from .models import User, Company


class RegistrationService:

    @staticmethod
    def is_email_valid(email):
        try:
            validate_email(email)
        except ValidationError:
            return False
        return True

    @staticmethod
    def email_in_use(email):
        return User.objects.filter(email=email).exists()

    def register(self, email):
        user = User.objects.create(email=email)
        user.set_password('Test')
        user.save()
        return Company.objects.create(user=user)

    def get_or_create_company(self, email):
        if not self.email_in_use(email):
            company = self.register(email)
        else:
            company = Company.objects.get(user__email=email)
        return company

    def save_or_register(self, email, data):
        company = self.get_or_create_company(email)

        company.name = data.get('company', '')
        company.commercial_name = data.get('commercial_name', '')
        company.address = data.get('address', '')
        company.activity = data.get('activity', '')
        company.director_first_name = data.get('director_first_name', '')
        company.director_last_name = data.get('director_last_name', '')
        company.save()
        return company

    def get_user(self, data):
        id = data.get('id', '')
        if id != '':
            try:
                return User.objects.get(pk=id)
            except User.DoesNotExist:
                pass
        return None

    def process_data(self, data):
        user = self.get_user(data)
        email = data.get('email', '')

        if not RegistrationService.is_email_valid(email):
            raise ValidationError('Email is in wrong format!')

        if user is None and self.email_in_use(email):
            raise ValidationError('Email in use!')

        return self.save_or_register(email, data)

{"user": {
{% if user.id %}
    "id": "{{ user.id }}",
    "email": "{{ user.email }}",
    "company": "{{ user.company.name }}",
    "commercial_name": "{{ user.company.commercial_name }}",
    "address": "{{ user.company.address }}",
    "activity": "{{ user.company.activity }}",
    "director_first_name": "{{ user.company.director_first_name }}",
    "director_last_name": "{{ user.company.director_last_name }}"
{% else %}
    "id": "",
    "email": "",
    "company": "",
    "commercial_name": "",
    "address": "",
    "activity": "",
    "director_first_name": "",
    "director_last_name": ""
{% endif %}
    },
"message": "{{ message }}"
}
(function() {
    'use strict';

    let component = {
        controller: Controller,
        templateUrl: '../static/registration/js/app/components/panel-dashboard.component.html',
        bindings: {
            'page': '=',
            'fillStatus': '&'
        }
    };


    Controller.$inject = [];

    function Controller () {
        let self = this;

        this.changePage = function(page) {
            if (page === self.fillStatus()) {
                switch (page) {
                    case 1:
                        self.page = 'info';
                        break;
                    case 2:
                        self.page = 'activity';
                        break;
                    case 3:
                        self.page = 'director';
                        break;
                }
            }
        }
    }

    angular.
        module('simplitoo').
        component('panelDashboard', component);

})();
(function() {
    'use strict';

    let component = {
        controller: Controller,
        templateUrl: '../static/registration/js/app/components/panel-info.component.html',
        bindings: {
            'page': '=',
            'user': '=',
            'sendData': '&'
        }
    };


    Controller.$inject = [];

    function Controller () {

        this.validateForm = () => {
            this.sendData();
            this.page = 'dashboard';
        };
    }

    angular.
        module('simplitoo').
        component('panelInfo', component);

})();
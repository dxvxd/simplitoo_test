(function() {
    'use strict';

    let component = {
        controller: Controller,
        templateUrl: '../static/registration/js/app/components/panel-director.component.html',
        bindings: {
            'page': '=',
            'user': '=',
            'sendData': '&'
        }
    };


    Controller.$inject = [];

    function Controller () {
        this.validateForm = () => {
            this.sendData();
            this.page = 'congrats';
        };
    }

    angular.
        module('simplitoo').
        component('panelDirector', component);

})();
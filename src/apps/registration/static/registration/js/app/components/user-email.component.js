(function() {
    'use strict';

    let component = {
        controller: Controller,
        templateUrl: '../static/registration/js/app/components/user-email.component.html',
        bindings: {
            'email': '=',
            'sendData': '&'
        }
    };


    Controller.$inject = [];

    function Controller () {
        this.new_email = '';

        this.sendRegister = () => {
            this.email = this.new_email;
            this.sendData();
        };
    }

    angular.
        module('simplitoo').
        component('userEmail', component);

})();
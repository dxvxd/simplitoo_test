(function() {
    'use strict';

    let component = {
        controller: Controller,
        templateUrl: '../static/registration/js/app/components/panel-congrats.component.html',
        bindings: {
            'user': '=',
            'sendData': '&'
        }
    };


    Controller.$inject = [];

    function Controller () {
        this.new_email = '';

        this.sendRegister = () => {
            this.user.email = this.new_email;
            this.sendData();
        };
    }

    angular.
        module('simplitoo').
        component('panelCongrats', component);

})();
(function () {
    'use strict';

    angular.module('simplitoo', [])
        .config(['$httpProvider', '$locationProvider', function ($httpProvider, $locationProvider) {
            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
            $httpProvider.defaults.xsrfCookieName = 'csrftoken';
            $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

            $locationProvider.html5Mode(true);
        }])
        .controller('StartController', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {
            let self = this;
            this.ready = false;
            this.user = {};

            this.getCurrentPage = function() {
                console.log(self.user);
                if (self.fillStatus() === 4) {
                    return 'congrats';
                }

                return 'dashboard';
            };

            this.fillStatus = function() {
                if (self.user.company === '' || self.user.commercial_name === '' || self.user.address === '') {
                    return 1;
                }
                if (self.user.activity === '') {
                    return 2;
                }
                if (self.user.director_first_name === '' || self.user.director_last_name === '') {
                    return 3;
                }
                return 4;
            };

            $http.get('/register/api/')
                .then(function (response) {
                    self.user = response.data.user;
                    self.page = self.getCurrentPage();
                });

            this.sendData = function () {
                $timeout(function(){
                    if (self.user.email !== '') {
                        $http({
                                url: '/register/api/',
                                method: 'POST',
                                data: self.user
                            })
                            .then(function (response) {
                                if (response.data.message !== 'ok') {
                                    alert(response.data.message);
                                    self.user.email = '';
                                }
                            });
                    }
                });
            };
        }]);

})();
